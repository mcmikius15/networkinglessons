//
//  CollectionViewCell.swift
//  Networking
//
//  Created by Michail Bondarenko on 2/26/19.
//  Copyright © 2019 Michail Bondarenko. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var label: UILabel!
}
